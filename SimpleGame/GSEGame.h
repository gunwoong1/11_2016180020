#pragma once
#include "Renderer.h"
#include "GSEObject.h"
#include "GSEGlobals.h"
#include "Sound.h"
class GSEGame
{
public:
	GSEGame();
	~GSEGame();

	void tempclass();

	void RenderScene();
	int AddObject(float x, float y, float depth,
		float sx, float sy,
		float velX, float velY,
		float accX, float accY,
		float mass);
	void DeleteObject(int index);
	void Update(float elapsedTimeInSec, GSEInputs* inputs);

private:
	bool AABBCollision(GSEObject* a, GSEObject* b);
	bool ProcessCollision(GSEObject* a, GSEObject* b);
	void AdjustPosition(GSEObject* a, GSEObject* b);
	void DoGarbageCollect();

	Renderer* m_renderer = NULL;
	Renderer* m_renderer2 = NULL;
	Sound* m_Sound = NULL;

	GSEObject* m_Objects[GSE_MAX_OBJECTS];
	int m_HeroID = -1;




	int m_HeroTexture = -1;
	int m_MonsterTexture = -1;
	int m_MonsterTexture2 = -1;
	int m_MonsterTexture3 = -1;
	int m_BrickTexture = -1;
	int m_SpriteTexture = -1;
	int m_SpriteTexture2 = -1;
	int m_attackTexture = -1;
	int m_attackTextureup = -1;
	int m_attackTextureleft = -1;
	int m_attackTexturedown = -1;



	//백그라운드 이미지
	int m_BGTexture = -1;
	int m_TitleTexture = -1;
	int m_EndingTexture = -1;


	//사운드
	int m_BGSound = -1;//배경
	int m_SwordSound = -1;//공격
	
	int m_crushSound = -1;//부셔질때
	int m_endingSound = -1;//엔딩음
	


};

