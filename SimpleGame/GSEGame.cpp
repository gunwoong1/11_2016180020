#include "stdafx.h"
#include "GSEGame.h"
#include "math.h"
int temp = 0;
int tempy = 0;
bool CHECK = false;
bool TITLE = true;//타이틀
bool GAME = false;//게임장면
bool ENDINGSOUND = false;//엔딩음악
bool END = false;//엔딩
float herox = -6.8;

void GSEGame::Update(float elapsedTimeInSec, GSEInputs* inputs)
{
	//do garbage collecting
	DoGarbageCollect();

	GSEUpdateParams othersParam;
	GSEUpdateParams heroParam;
	memset(&othersParam, 0, sizeof(GSEUpdateParams));
	memset(&heroParam, 0, sizeof(GSEUpdateParams));

	//calc force
	float forceAmount = 400.f;
	if (inputs->KEY_W)
	{
		heroParam.forceY += 20 * forceAmount;

	}
	if (inputs->KEY_A)
	{
		heroParam.forceX -= forceAmount;
		m_Objects[m_HeroID]->SetTextureID(m_SpriteTexture2);


	}
	if (inputs->KEY_S)
	{
		heroParam.forceY -= forceAmount;
	}
	if (inputs->KEY_D)
	{
		heroParam.forceX += forceAmount;
		m_Objects[m_HeroID]->SetTextureID(m_SpriteTexture);


	}

	if (inputs->TITLE)//게임시작
	{
		m_Sound->PlayBGSound(m_BGSound, true, 0.7f);
		TITLE = false;
		GAME = true;
	}



	//sword
	float swordPosX = 0.f;
	float swordPosY = 0.f;

	if (inputs->ARROW_LEFT) swordPosX += -0.7f;
	if (inputs->ARROW_RIGHT) swordPosX += 0.7f;
	if (inputs->ARROW_DOWN) swordPosY += -1.f;
	if (inputs->ARROW_UP) swordPosY += 1.f;

	float swordDirSize = sqrtf(swordPosX * swordPosX + swordPosY * swordPosY);
	if (swordDirSize > 0.f)
	{
		float norDirX = (swordPosX / swordDirSize);
		float norDirY = (swordPosY / swordDirSize);

		float aX, aY, asX, asY;
		float bX, bY, bsX, bsY;
		float temp;

		m_Objects[m_HeroID]->GetPosition(&aX, &aY, &temp);
		m_Objects[m_HeroID]->GetSize(&asX, &asY);

		if (GAME == true)
		{
			//칼 만드는 부분
			if (m_Objects[m_HeroID]->GetRemainingCoolTime() < 0.f)
			{
				int swordID = AddObject(0.f, 0.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
				m_Objects[swordID]->SetParentID(m_HeroID);
				m_Objects[swordID]->SetType(GSEObjectType::TYPE_SWORD);
				m_Objects[swordID]->SetRelPosition(norDirX, norDirY, 0.f);
				m_Objects[swordID]->SetStickToParent(true);
				m_Objects[swordID]->SetLife(100.f);
				m_Objects[swordID]->SetLifeTime(0.3f); //0.2 초 후 자동 삭제.

				m_Objects[m_HeroID]->ResetRemainingCoolTime();
				m_Objects[swordID]->SetTextureID(m_attackTexture);
				if (inputs->ARROW_RIGHT)
				{
					m_Objects[m_HeroID]->SetTextureID(m_SpriteTexture);

				}
				if (inputs->ARROW_UP)
				{
					m_Objects[swordID]->SetTextureID(m_attackTextureup);
					//CRUSHMONSTER1 = false;
				}
				if (inputs->ARROW_LEFT)
				{
					m_Objects[swordID]->SetTextureID(m_attackTextureleft);
					m_Objects[m_HeroID]->SetTextureID(m_SpriteTexture2);
				}
				if (inputs->ARROW_DOWN)
				{
					m_Objects[swordID]->SetTextureID(m_attackTexturedown);
				}
				m_Sound->PlayShortSound(m_SwordSound, false, 0.3f);//칼 생성시마다 사운드생성
			}

		}

	}
	//Processing collision
	bool isCollide[GSE_MAX_OBJECTS];
	memset(isCollide, 0, sizeof(bool) * GSE_MAX_OBJECTS);
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		for (int j = i + 1; j < GSE_MAX_OBJECTS; j++)
		{
			if (m_Objects[i] != NULL && m_Objects[j] != NULL
				&&
				m_Objects[i]->GetApplyPhysics() && m_Objects[j]->GetApplyPhysics())
			{
				bool collide = ProcessCollision(m_Objects[i], m_Objects[j]);
				if (collide)
				{
					isCollide[i] = true;
					isCollide[j] = true;
				}
			}
		}
	}

	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (!isCollide[i])
				m_Objects[i]->SetState(GSEObjectState::STATE_FALLING);
		}
	}

	//Update All Objects
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (i == m_HeroID)
			{
				m_Objects[i]->Update(elapsedTimeInSec, &heroParam);
			}
			else
			{
				if (m_Objects[i]->GetStickToParent())
				{
					float posX, posY, depth;
					float relPosX, relPosY, relDepth;
					int parentID = m_Objects[i]->GetParentID();
					m_Objects[parentID]->GetPosition(&posX, &posY, &depth);
					m_Objects[i]->GetRelPosition(&relPosX, &relPosY, &relDepth);
					m_Objects[i]->SetPosition(posX + relPosX, posY + relPosY, depth + relDepth);
					m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
				}
				else
				{
					m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
				}
			}
		}
	}

	float x, y, z;
	m_Objects[m_HeroID]->GetPosition(&x, &y, &z);//캐릭터를 따라서 카메라를 이동
	m_renderer->SetCameraPos(x * 20.0f, y * 5.f);

	if (x > 5.5 && y > -1.3)
	{
		END = true;
		GAME = false;
		m_Sound->DeleteBGSound(m_BGSound);
		ENDINGSOUND = true;
	}

}

GSEGame::GSEGame()
{
	if (ENDINGSOUND == true)
	{
		m_Sound->PlayShortSound(m_endingSound, false, 0.07f);
	}

	//Renderer initialize
	m_renderer = new Renderer(GSE_WINDOWSIZE_X, GSE_WINDOWSIZE_Y);
	m_renderer2 = new Renderer(GSE_WINDOWSIZE_X, GSE_WINDOWSIZE_Y);

	m_Sound = new Sound();

	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		m_Objects[i] = NULL;
	}

	m_HeroTexture = m_renderer->GenPngTexture("a.png"); //재사용 가능!!
	m_MonsterTexture = m_renderer->GenPngTexture("monster1.png"); //재사용 가능!!
	m_MonsterTexture2 = m_renderer->GenPngTexture("monster2.png"); //재사용 가능!!


	m_BrickTexture = m_renderer->GenPngTexture("brick.png"); //재사용 가능!!
	m_SpriteTexture = m_renderer->GenPngTexture("spriteSheet.png"); //재사용 가능!!
	m_SpriteTexture2 = m_renderer->GenPngTexture("spriteSheetleft.png"); //재사용 가능!!
	m_BGTexture = m_renderer->GenPngTexture("background.png");//배경이미지

	

	m_TitleTexture = m_renderer->GenPngTexture("title.png");//타이틀
	m_EndingTexture = m_renderer->GenPngTexture("ending.png");//엔딩
	
	
	m_attackTexture = m_renderer->GenPngTexture("attackSheet.png");//오른쪽공격
	m_attackTextureup = m_renderer->GenPngTexture("attackSheetup.png");//위
	m_attackTextureleft = m_renderer->GenPngTexture("attackSheetleft.png");//왼
	m_attackTexturedown = m_renderer->GenPngTexture("attackSheetdown.png");//아래

	//Create Hero

		m_HeroID = AddObject(herox,-3.6,0, 1, 1, 0, 0, 0, 0, 55);//히어로 초기위치
		m_Objects[m_HeroID]->SetType(GSEObjectType::TYPE_HERO);
		m_Objects[m_HeroID]->SetApplyPhysics(true);
		m_Objects[m_HeroID]->SetLife(100000000.f);
		m_Objects[m_HeroID]->SetLifeTime(100000000.f);
		m_Objects[m_HeroID]->SetTextureID(m_SpriteTexture);


		
		
	//몬스터생성
		
	//1번
		int monster = AddObject(-1.25, -4.0, 0, 2, 0.3, 0, 0, 0, 0, 1000);
		monster = AddObject(-4.4, -3.2, 1, 1, 1, 1, 1, 1, 1, 1000);
		m_Objects[monster]->SetType(GSEObjectType::TYPE_MOVABLE);
		m_Objects[monster]->SetApplyPhysics(true);
		m_Objects[monster]->SetLife(1000000000.f);
		m_Objects[monster]->SetLifeTime(10000000.0f);
		m_Objects[monster]->SetTextureID(m_MonsterTexture);

		

	//2번
		int monster2 = AddObject(-1.25, -4.0, 0, 2, 0.3, 0, 0, 0, 0, 100);
		monster2 = AddObject(-2.3, -2.8, 1, 1, 1, 1, 1, 1, 1, 100);
		m_Objects[monster2]->SetType(GSEObjectType::TYPE_MOVABLE);
		
		m_Objects[monster2]->SetApplyPhysics(true);
		m_Objects[monster2]->SetLife(100000000.f);
		m_Objects[monster2]->SetLifeTime(100000000.f);
		m_Objects[monster2]->SetTextureID(m_MonsterTexture);

		//3번
		int monster5 = AddObject(-1.25, -4.0, 0, 2, 0.3, 0, 0, 0, 0, 100);
		monster5 = AddObject(-0.2, -2.4, 1, 1, 1, 1, 1, 1, 1, 100);
		m_Objects[monster5]->SetType(GSEObjectType::TYPE_MOVABLE);
		m_Objects[monster5]->SetApplyPhysics(true);
		m_Objects[monster5]->SetLife(1000000000.f);
		m_Objects[monster5]->SetLifeTime(1000000000.f);

		m_Objects[monster5]->SetTextureID(m_MonsterTexture);

		//4번
		int monster4 = AddObject(-1.25, -4.0, 0, 2, 0.3, 0, 0, 0, 0, 100);
		monster4 = AddObject(1.8, -2.0, 1, 1, 1, 1, 1, 1, 1, 100);
		m_Objects[monster4]->SetType(GSEObjectType::TYPE_MOVABLE);
		m_Objects[monster4]->SetApplyPhysics(true);
		m_Objects[monster4]->SetLife(1000000000.f);
		m_Objects[monster4]->SetLifeTime(1000000000.f);

		m_Objects[monster4]->SetTextureID(m_MonsterTexture);


		//5번
		int monster3 = AddObject(-1.25, -4.0, 0, 2, 0.3, 0, 0, 0, 0, 100);
		monster3= AddObject(3.9, -1.6, 1, 1, 1, 1, 1, 1, 1, 100);
		m_Objects[monster3]->SetType(GSEObjectType::TYPE_MOVABLE);
		m_Objects[monster3]->SetApplyPhysics(true);
		m_Objects[monster3]->SetLife(1000000000.f);
		m_Objects[monster3]->SetLifeTime(1000000000.f);

		m_Objects[monster3]->SetTextureID(m_MonsterTexture);


		//6번
		int monster6 = AddObject(-1.25, -4.0, 0, 2, 0.3, 0, 0, 0, 0, 100);
		monster6 = AddObject(6.0, -0.8, 1, 1, 1, 1, 1, 1, 1, 100);
		m_Objects[monster6]->SetType(GSEObjectType::TYPE_END);
		m_Objects[monster6]->SetApplyPhysics(true);
		m_Objects[monster6]->SetLife(1000000000.f);
		m_Objects[monster6]->SetLifeTime(1000000000.f);

		m_Objects[monster6]->SetTextureID(m_MonsterTexture2);


		

	//바닥생성
	int floor = AddObject(-1.25, -2.5, 0, 2, 0.3, 0, 0, 0, 0, 10000);



	floor = AddObject(-5.0, -4.2, 0, 50, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(1000000000.f);
	m_Objects[floor]->SetLifeTime(1000000000.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);

	//1번블록
	floor = AddObject(-4.6, -3.5, 0, 2, 0.15, 0, 0,0, 0, 10000); 
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100.f);
	m_Objects[floor]->SetLifeTime(12.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);

	//2번블록
	floor = AddObject(-2.4, -3.1, 0, 2, 0.15, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100.f);
	m_Objects[floor]->SetLifeTime(24.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);

	//3번블록
	floor = AddObject(-0.2, -2.7, 0, 2, 0.15, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100.f);
	m_Objects[floor]->SetLifeTime(36.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);

	//4번블록
	floor = AddObject(2.0, -2.3, 0, 2, 0.15, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100.f);
	m_Objects[floor]->SetLifeTime(48.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);


	//5번블록
	floor = AddObject(4.0, -1.9, 0, 2, 0.15, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100.f);
	m_Objects[floor]->SetLifeTime(60.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);

	//6번블록
	floor = AddObject(6.0, -1.4, 0, 2, 0.15, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000.f);
	m_Objects[floor]->SetLifeTime(1000000.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);

	//7번블록
	floor = AddObject(8.0, -1.0, 0, 2, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(1000000000.f);
	m_Objects[floor]->SetLifeTime(10000000000.f);
	m_Objects[floor]->SetTextureID(m_BrickTexture);


		m_BGSound = m_Sound->CreateBGSound("bgsound.mp3");//배경음악같이 긴 음악파일
	/////////사운드처리////////////////////////////
	
	m_SwordSound = m_Sound->CreateShortSound("swordsound.mp3");//효과음같이 짧은 음악파일
	m_endingSound= m_Sound->CreateShortSound("victory.wav");//효과음같이 짧은 음악파일
	
	
}

GSEGame::~GSEGame()
{
	//Renderer delete
}

//충돌효과부분
bool GSEGame::ProcessCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType, bType;
	a->GetType(&aType);
	b->GetType(&bType);

	bool isCollide = AABBCollision(a, b);
	if (isCollide)
	{
		
		//do something
		if (aType == GSEObjectType::TYPE_FIXED || bType == GSEObjectType::TYPE_FIXED)
		{
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
			CHECK = false;
			
		}

	/*	if (aType == GSEObjectType::TYPE_MOVABLE || bType == GSEObjectType::TYPE_HERO)
		{
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
		}*/


		
		else if (aType == GSEObjectType::TYPE_HERO || bType == GSEObjectType::TYPE_MOVABLE)
		{
			
				a->SetState(GSEObjectState::STATE_GROUND);
				b->SetState(GSEObjectState::STATE_GROUND);
			
				
		}
		
		else if (aType == GSEObjectType::TYPE_HERO|| bType == GSEObjectType::TYPE_END)
		{
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
			
			END = true;
			GAME = false;
		}
	}
	

	return isCollide;
}

//충돌처리부분
bool GSEGame::AABBCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if (aMinX > bMaxX) // || fabs(aMinX-bMaxX)<FLT_EPSILON
	{
		
		return false;
	}
	if (aMaxX < bMinX)
	{
		
		return false;
	}
	if (aMinY > bMaxY)
	{
		
		return false;
	}
	if (aMaxY < bMinY)
	{
		
		return false;
	}

	
	AdjustPosition(a, b);
	return true;
}

void GSEGame::AdjustPosition(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if ((aType == GSEObjectType::TYPE_MOVABLE|| aType == GSEObjectType::TYPE_MOVABLE)
		&&
		bType == GSEObjectType::TYPE_HERO)
	{
		if (aMaxY > bMaxY)
		{
			aY = aY + (bMaxY - aMinY);
			
			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);

			
		
		}
		else
		{
			aY = aY - (aMaxY - bMinY);

			a->SetPosition(aX, aY, 0.f);
			
			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
	
		}
		
	}
	else if (
		(bType == GSEObjectType::TYPE_MOVABLE || bType == GSEObjectType::TYPE_MOVABLE)
		&&
		(aType == GSEObjectType::TYPE_HERO)
		)
	{
		if (!(bMaxY > aMaxY && bMinY < aMinY))
		{
			if (bMaxY > aMaxY)
			{
				bY = bY + (aMaxY - bMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(0.f,vy);
			
			}
			else
			{
				bY= bY - (bMaxY - aMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(0.f,vy);
				
			}
		}
		
	}
	////


	else if ((aType == GSEObjectType::TYPE_END || aType == GSEObjectType::TYPE_END)
		&&
		bType == GSEObjectType::TYPE_HERO)
	{
		if (aMaxY > bMaxY)
		{
			aY = aY + (bMaxY - aMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
		

		}
		else
		{
			aY = aY - (aMaxY - bMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);

		}

	}
	else if (
		(bType == GSEObjectType::TYPE_END|| bType == GSEObjectType::TYPE_END)
		&&
		(aType == GSEObjectType::TYPE_HERO)
		)
	{
		if (!(bMaxY > aMaxY && bMinY < aMinY))
		{
			if (bMaxY > aMaxY)
			{
				bY = bY + (aMaxY - bMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(0.f, vy);
				
			}
			else
			{
				bY = bY - (bMaxY - aMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(0.f, vy);

			}
		}

	}
}

void GSEGame::DoGarbageCollect()
{
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			float life = m_Objects[i]->GetLife();
			float lifeTime = m_Objects[i]->GetLifeTime();
			if (life < 0.f || lifeTime < 0.f)
			{
			

				DeleteObject(i);
			}
		}
	}
}

void GSEGame::tempclass()
{

	
}

	float tempcount = 0;

void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Renderer Test
	//m_renderer->DrawSolidRect(0, 0, 0, 1000, 1, 0, 1, 1);

	if (TITLE == true)
	{
		m_renderer->DrawTextureRect(-135, -10, 0, 1200, 800, 1, 1, 1, 1, 1, m_TitleTexture);
	}

	if (GAME == true)
	{
		//배경 이미지 그리기
		m_renderer->DrawGround(0, 0, 0, 2400, 1600, 1, 1, 1, 1, 1, m_BGTexture);
		//Draw All Objects
		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			if (m_Objects[i] != NULL)
			{
				float x, y, depth;
				m_Objects[i]->GetPosition(&x, &y, &depth);
				float sx, sy;
				m_Objects[i]->GetSize(&sx, &sy);

				int textureID = m_Objects[i]->GetTextureID();

				//meter to pixel
				x = x * 100.f;
				y = y * 100.f;
				sx = sx * 100.f;
				sy = sy * 100.f;

				if (textureID < 0)
				{
					m_renderer->DrawSolidRect(x, y, depth, sx, sy, 0.f, 1, 0, 1, 1);
				}
				else
				{

					m_renderer->DrawTextureRectAnim
					(
						x, y, depth,
						sx, sy, 1.f,
						1.f, 1.f, 1.f, 1.f,
						textureID,
						8,
						12,
						temp,
						0);

					//애니메이션 부드럽게
					tempcount += 0.015;
					temp = tempcount;
					temp = temp % 8;

			
				}
			}
		}
	}

	//엔딩
	if (END == true)
	{
		m_renderer->DrawTextureRect(-10, 0, 0, 2400, 1200, 1, 1, 1, 1, 1,m_EndingTexture);
		
	}
}

int GSEGame::AddObject(float x, float y, float depth, 
	float sx, float sy,
	float velX, float velY,
	float accX, float accY,
	float mass)
{
	//find empty slot
	int index = -1;
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
		{
			index = i;
			break;
		}
	}

	if (index < 0)
	{
		std::cout << "No empty object slot.. " << std::endl;
		return -1;
	}

	m_Objects[index] = new GSEObject();
	m_Objects[index]->SetPosition(x, y, depth);
	m_Objects[index]->SetSize(sx, sy);
	m_Objects[index]->SetVel(velX, velY);
	m_Objects[index]->SetAcc(accX, accY);
	m_Objects[index]->SetMass(mass);

	return index;

	
}

void GSEGame::DeleteObject(int index)
{
	if (m_Objects[index] != NULL)
	{
		delete m_Objects[index];
		m_Objects[index] = NULL;
	}
	else
	{
		std::cout << "Try to delete NULL object : " << index << std::endl;
	}
}
