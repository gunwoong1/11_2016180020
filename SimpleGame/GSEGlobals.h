#pragma once

#define GSE_MAX_OBJECTS 1000
#define GSE_GRAVITY 9.8f
#define GSE_MOVE 50.0f
#define GSE_WINDOWSIZE_X 1200
#define GSE_WINDOWSIZE_Y 800


typedef struct GSEInputs
{
	//공격
	bool ARROW_UP;
	bool ARROW_DOWN;
	bool ARROW_LEFT;
	bool ARROW_RIGHT;

	//이동
	bool KEY_W;
	bool KEY_A;
	bool KEY_S;
	bool KEY_D;

	//시작
	bool TITLE;

	//재시작
	bool RETURN;

	

};

typedef struct GSEUpdateParams
{
	float forceX;
	float forceY;
};

enum GSEObjectType {
	TYPE_HERO,
	TYPE_MOVABLE,
	TYPE_FIXED,
	TYPE_BULLET,
	TYPE_SWORD,
	TYPE_END,
};

enum GSEObjectState {
	STATE_GROUND,
	STATE_FALLING,
	STATE_FLY,
	STATE_SWIMMING,
};