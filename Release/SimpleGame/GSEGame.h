#pragma once
#include "Renderer.h"
#include"GSEobject.h"
#include "GSEGlobals.h"

#define GSE_MAX_OBJECTS 1000

class GSEGame
{
public:
	GSEGame();
    ~GSEGame();
	
	void RenderScene();
	int Addobject(float x, float y, float depth, float sx, float sy, float velX, float velY, float accX, float accY, float mass);
	void Deleteobject(int index);
	void Update(float elapsedTimeInSec,GSEinputs*inputs);

	
private:

	Renderer* m_renderer = NULL;
	GSEobject* m_objects[GSE_MAX_OBJECTS];
	int m_HeroID = -1;//�����

};

