#include "stdafx.h"
#include "GSEobject.h"


GSEobject::GSEobject()
{

	m_PositionX = -1000000;
	m_PositionY = -1000000;
	m_Depth = -1000000;
	m_SizeX= -1000000;
	m_SizeY= -1000000;
	m_AccX= -1000000;
	m_AccY= -1000000;
	m_VelX = -1000000;;
	m_VelY= -1000000;
	m_Mass= -1000000;

}
GSEobject::~GSEobject()
{

}

void GSEobject::Update(float elapsedTimeInSec, GSEUpdateParams* param)
{
	float t = elapsedTimeInSec;
	float tt = elapsedTimeInSec * elapsedTimeInSec;

	//calce temporary
	float accX = param->forceX / m_Mass;
	float accY = param->forceY / m_Mass;

	//sum with objec'acc
	accX += m_AccX;
	accY += m_AccY;

	//�߷°��
	accY -= GSE_GRAVITY;

	//update position 
	m_PositionX = m_PositionX + m_VelX * t + 0.5f * accX * tt;
	m_PositionY = m_PositionY + m_VelY * t + 0.5f * accY * tt;

	//update velocity
	m_VelX = m_VelX + accX * t;
	m_VelY = m_VelY + accY * t;


}
void GSEobject::SetPosition(float x,float y,float depth)
{
	m_PositionX = x;
	m_PositionY = y;
	m_Depth = depth;
}

void GSEobject::GetPosition(float* x, float* y, float* depth)
{
	*x = m_PositionX;
	*y = m_PositionY;
	*depth = m_Depth;
}

void GSEobject::SetSize(float sx,float sy)
{
	m_SizeX = sx;
	m_SizeY = sy;
}

void GSEobject::SetVel(float x, float y)
{
	m_VelX = x;
	m_VelY = y;
}
void GSEobject::SetAcc(float x, float y)
{
	m_AccX = x;
	m_AccY = y;
}

void GSEobject::SetMass(float x)
{
	m_Mass = x;
}

void GSEobject::GetSize(float* sx, float* sy)
{
	*sx = m_SizeX;
	*sy = m_SizeY;
}

void GSEobject::GetVel(float* x, float* y)
{
	*x = m_VelX;
	*y = m_VelY;

}
void GSEobject::GetAcc(float* x, float* y)
{
	*x = m_AccX;
	*y = m_AccY;
}
void GSEobject::GetMass(float* x)
{
	*x = m_Mass;
}