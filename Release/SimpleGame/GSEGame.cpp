#include "stdafx.h"
#include "GSEGame.h"



//이곳에 객체추가
GSEGame::GSEGame()
{
	//Renderer initialize
	m_renderer = new Renderer(500, 500);
	
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		m_objects[i] == NULL;
	}
	//히어로생성
	m_HeroID = Addobject(0, 0, 0, 1,1, 0, 0, 0, 0, 20);

	int floor = Addobject(-1.25, -2.5, 0, 2, 0.3, 0, 0, 0, 0, 10000);


}

GSEGame::~GSEGame()
{
	//Renderer delete


}

//중요한 함수들
void GSEGame::Update(float elapsedTimeInSec,GSEinputs* inputs)
{

	GSEUpdateParams othersParam;
	GSEUpdateParams heroParam;
	memset(&othersParam, 0, sizeof(GSEUpdateParams));
	memset(&heroParam, 0, sizeof(GSEUpdateParams));

	//calc force
	float forceAmount = 100.f;//객체이동속도에 영향
	if (inputs->KEY_W)
	{
		heroParam.forceY += forceAmount;
	}
	if (inputs->KEY_A)
	{
		heroParam.forceX -= forceAmount;
	}
	if (inputs->KEY_S)
	{
		heroParam.forceY -= forceAmount;
	}
	if (inputs->KEY_D)
	{
		heroParam.forceX += forceAmount;
	}

	//모든오브젝트
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_objects[i] != NULL)
		{
			if (i == m_HeroID)
			{
				m_objects[i]->Update(elapsedTimeInSec,&heroParam);
			}
			else
			{

			m_objects[i]->Update(elapsedTimeInSec,&othersParam);
			}
		
		}
	}
}


void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);


	//m_renderer->DrawSolidRect(0, 0, 0, 100, 1, 0, 1, 1);
	//m_renderer->DrawSolidRect(0, 0, 0, 100, 1, 1,0 , 1);

	//Draw all objects
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_objects[i] != NULL)
		{
			float x, y, depth;
			m_objects[i]->GetPosition(&x, &y, &depth);
			float sx, sy;
			m_objects[i]->GetSize(&sx, &sy);

			//미터

			x = x* 100.f;
			y = y * 100.f;
			sx = sx * 100.f;
			sy = sy * 100.f;

			m_renderer->DrawSolidRect(x, y, depth, sx,sy, 1, 0, 1, 1);
		}
	}


}
/////////////////////////////////////////////////////////////////
int GSEGame::Addobject(float x, float y, float depth,float sx,float sy,float velX,float velY,float accX,float accY,float mass)
{
	//빈슬롯 찾기
	int index = -1;
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_objects[i] == NULL)
		{
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		std::cout << "No empty object slot.." << std::endl;

		return -1;
	}

	m_objects[index] = new GSEobject();
	m_objects[index]->SetPosition(x, y, depth);
	m_objects[index]->SetSize(sx, sy);
	m_objects[index]->SetVel(velX,velY);
	m_objects[index]->SetAcc(accX,accY);
	m_objects[index]->SetMass(mass);

	return index;
}


void GSEGame::Deleteobject(int index)
{
	if (m_objects[index] != NULL)
	{
		delete m_objects[index];
		m_objects[index] = NULL;
	}

	else
	{
		std::cout << "Try to delete NULL object:" << index << std::endl;
	}

}


