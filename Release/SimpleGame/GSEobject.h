#pragma once

#include "GSEGlobals.h"

class GSEobject
{
public:

	GSEobject();
	~GSEobject();

	void Update(float elapsedTimeInSec,GSEUpdateParams*param);

	void SetPosition(float x, float y, float depth); 
	void SetSize(float sx, float sy);
	void SetVel(float x, float y);
	void SetAcc(float x, float y);
	void SetMass(float x);



	void GetPosition(float *x, float *y,float *depth);
	void GetSize(float* sx, float* sy);
	void GetVel(float* x, float* y);
	void GetAcc(float* x, float* y);
	void GetMass(float* x);


private:

	float m_PositionX, m_PositionY;//위치
	float m_Depth;
	float m_SizeX, m_SizeY;//크기
	float m_VelX, m_VelY;//속도
	float m_AccX, m_AccY;//가속도
	float m_Mass;  //질량

};

